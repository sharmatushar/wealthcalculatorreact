import React from 'react';
import '../App.css';
import {Link} from 'react-router-dom'; 

function Nav(){
    return(
        <nav>
            <h3>Logo</h3>
            <ul className="nav-Links">
                <Link className="nav-Items" to='/about'>
                    <li >About</li>
                </Link>
                <Link className="nav-Items" to='/wealth-calculator'>
                    <li >Wealth Calculator</li>
                </Link>
                <Link className="nav-Items" to='/fortnite-items'>
                    <li>Fortnite Items</li>
                </Link>
            </ul>
        </nav>
    );
}
export default Nav;