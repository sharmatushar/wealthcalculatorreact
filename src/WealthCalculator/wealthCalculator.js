import '../App.css';
import React from 'react'
import SubApp from './wealthCalculatorFunctionalities';
import Heading from './heading';
function wealthCalculator(){
    return(
      <div>
        <Heading />
        <SubApp />
      </div>
    );
  }
export default wealthCalculator;