import '../App.css';
import React from 'react'
import MIL from '../constants/million'
import _map from 'lodash/map'
import { Currency } from 'react-intl-number-format/dist/index.js'
class ShowUsers extends React.Component{
    render(){
      const {users} = this.props;
      const {millionaireToggle} = this.props;
        return(
            <table id="personTable">
            <tbody>
              {_map(users,(person) => {
                console.log("here");
                if(millionaireToggle){
                    if(person.wealth>=MIL){
                        return (
                          <tr key={person.wealth}>
                            <td>{person.name}</td>
                            <td><Currency locale="en-US" currency="USD">{person.wealth}</Currency></td>
                          </tr>
                          )
                    }
                }else{
                    return (
                    <tr key={person.wealth}>
                      <td>{person.name}</td>
                      <td><Currency locale="en-US" currency="USD">{person.wealth}</Currency></td>
                    </tr>
                    )
                }
              })}
              </tbody>
            </table>
          );
    }
}

export default ShowUsers