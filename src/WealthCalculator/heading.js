import '../App.css';
import React from 'react'


class Heading extends React.Component {
    render() {
      const styles = {
        textAlign: 'center'
      };
      return React.createElement("h1", {
        style: styles
      }, "Wealth Calculator");
    }
  
  }
export default Heading