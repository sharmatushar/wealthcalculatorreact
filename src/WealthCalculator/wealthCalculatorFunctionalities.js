import '../App.css';
import React from 'react'
import TotalWealth from './totalWealth';
import ShowUsers from './ShowUsers'
import Buttons from './Buttons'
import MIL from '../constants/million'
import _map from 'lodash/map'
import _reduce from 'lodash/reduce'

function SubApp(){
    const [millionaireToggle, setMillionaireToggle] = React.useState(false);
    const [sortToggle, setSortToggle] = React.useState(false);
    const [totalWealthToggle, setTotalWealthToggle] = React.useState(false);
    const [totalWealth, setTotalWealth] = React.useState(0);
    const [users,setUsers] = React.useState([]);
    const [usersSorted, setUsersSorted] = React.useState([]);
    
    function addUser(){
      setTotalWealthToggle(false);
      fetchUser();
    }
  
    async function fetchUser(){
      try{
        console.log("In Fetch User");
        const randomUserPromise = await fetch('https://randomuser.me/api/');
        const randomUserData = await randomUserPromise.json();
        const randomUser = randomUserData.results[0];
        const userWealth = Math.floor(Math.random()*MIL);
        const person = {
            name : randomUser.name.first+" "+randomUser.name.last,
            wealth : userWealth
        }
        console.log(person);
        setUsers(users => [...users,person]);
      }catch(err){
        console.log("Error in Fetching Random User");
        alert(err);
      }
    }
  
    function showMillionaires(){
      if(!millionaireToggle){
        setMillionaireToggle(true);
      }else{
        setMillionaireToggle(false);
      }
      setTotalWealthToggle(false);
    }
  
    function sortRichest(){
      if(!sortToggle){
        setSortToggle(true);
        setUsersSorted([...users].sort((firstUser, secondUser) => secondUser.wealth-firstUser.wealth));
      }else{
        setSortToggle(false);
      }
      setTotalWealthToggle(false);
    }
  
    function calculateTotalWealth(){
      if(!totalWealthToggle){
        setTotalWealthToggle(true);
        const totalSum = _reduce(users,(sum,person)=>{
          if(millionaireToggle){
            if(person.wealth>MIL){
                return sum+person.wealth;
            }else{
              return sum;
            }
          }else{
            return sum+person.wealth;
          }
        },0);
        setTotalWealth(totalSum);
      }else{
        setTotalWealthToggle(false);
      }
    }
    
    const doubleMoney = ()=>{
        setTotalWealthToggle(false);
        const temp = [...users];
        _map(temp, (person) => {
          if(millionaireToggle){
            if(person.wealth>=MIL){
                person.wealth*=2;
            }
          }else{
              person.wealth*=2;
          }
        });
        setUsers(temp);
        setTotalWealthToggle(false);
    }

    return(
      <div className="row">
        <Buttons millionaireToggle={millionaireToggle} 
                sortToggle={sortToggle} 
                totalWealthToggle={totalWealthToggle}
                addUser={addUser}
                doubleMoney={doubleMoney}
                showMillionaires={showMillionaires}
                sortRichest={sortRichest}
                calculateTotalWealth={calculateTotalWealth}
        />
        <div className="column">
          <div className="tableHeading">Persons</div>
          <div className="tableHeading">Wealth</div>
          {users.length>0? <ShowUsers users={sortToggle ? usersSorted : users} millionaireToggle={millionaireToggle}/> : null}
          {totalWealthToggle ? <TotalWealth sum={totalWealth}/> : null}
        </div>
      </div>
    );
  }

  export default SubApp