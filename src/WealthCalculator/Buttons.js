import '../App.css';
import React from 'react';

function Buttons (props){
    const {millionaireToggle : milToggle} = props;
    const {sortToggle : srtToggle} = props;
    const {totalWealthToggle : totalToggle} = props;
    const {addUser} = props;
    const {doubleMoney} = props;
    const {sortRichest} = props;
    const {showMillionaires : showMil} = props;
    const {calculateTotalWealth : totalWealth} = props;

    const addUserClass = "btn "+ ((milToggle || srtToggle) ? "btn3" : "");
    const addUserDisable = milToggle || srtToggle;
    const doubleMoneyClass = "btn"
    const showMilClass = "btn "+ ((milToggle) ? "btn2" : "");
    const sortRichestClass = "btn " + (srtToggle ? 'btn2' : "");
    const totalWealthClass = "btn " + (totalToggle ? 'btn2' : "");
    
    return (
    <div className="btnColumn">
    <button className={addUserClass} id = "addUserBtn" disabled={addUserDisable} onClick = {addUser} >Add User 👨🏻‍💼</button>
    <button className={doubleMoneyClass} onClick={doubleMoney}>Double Money 🚀</button>
    <button className={showMilClass} id = "showMillionaireBtn" onClick = {showMil}>Show Only Millionaires 🕺🏻</button>
    <button className={sortRichestClass} id = "sortBtn" onClick={sortRichest}>Sort By Richest 💵</button>
    <button className={totalWealthClass} id = "totalWealthBtn" onClick={totalWealth}>Calculate Entire Wealth 💰</button>
    </div>
    );
}

export default Buttons