import '../App.css';
import React from 'react'

const TotalWealth = (prop) => { 
    return (
      <div>
        <div className="tableFooter">Total Wealth : </div>
        <div className="tableFooter">{"$ "+prop.sum.toLocaleString("en-US")}</div>
      </div>
    );
}

export default TotalWealth