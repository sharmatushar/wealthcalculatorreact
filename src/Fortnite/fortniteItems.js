import React from 'react'
import {Link} from 'react-router-dom'

function FortniteItems(){
    const[items,setItems] = React.useState([]);

    React.useEffect(()=>{
            fetchItems();
        },[])
    const fetchItems = async () => {
        const data = await fetch('https://fortnite-api.theapinetwork.com/items/popular');
        const items = await data.json();
        setItems(items.entries[1].entries);
    }

    return(
        <div className="fortnite-items-list">
            {items.map(item => {
                console.log(item);
                return (
                    <Link to={`/fortnite-items/${item.identifier}`} key={item.identifier}>
                        <h3 className="items">{item.name}</h3>
                    </Link>
                )
            })}
        </div>
    );
}
export default FortniteItems;
