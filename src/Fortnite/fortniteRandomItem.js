import React from 'react'

function FortniteRandomItem({match}){
    const[item,setItem] = React.useState({
        images:{}
    });

    React.useEffect(()=>{
            fetchItem();
            console.log(match);
        },[])
    const fetchItem = async () => {
        console.log("here")
        const data = await fetch(`https://fortnite-api.theapinetwork.com/item/get?id=${match.params.id}`);
        const randomItem = await data.json();
        setItem(randomItem.data.item);
    }

    return(
        <div className="random-item">
            <img src={item.images.information}></img>
            <h1>{item.name}</h1>
        </div>
    );
}
export default FortniteRandomItem;
