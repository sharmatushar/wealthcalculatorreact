import './App.css';
import React from 'react'
import Nav from './NavBar/Nav'
import Heading from './WealthCalculator/heading';
import wealthCalculator from './WealthCalculator/wealthCalculator';
import FortniteItems from './Fortnite/fortniteItems';
import FortniteRandomItem from './Fortnite/fortniteRandomItem';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App(){
  return(
    <Router>
      <div>
        <Nav />
          <Switch>
            <Route path="/" exact component={Heading}/>
            <Route path="/about" component={Heading}/>
            <Route path="/wealth-calculator" component={wealthCalculator}/>
            <Route path="/fortnite-items" exact component={FortniteItems}/>
            <Route path="/fortnite-items/:id" component={FortniteRandomItem}/>
          </Switch>
      </div>
    </Router>
  );
}

export default App;
